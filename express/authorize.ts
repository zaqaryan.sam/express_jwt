import * as express from 'express';
import { Request, Response, NextFunction} from 'express';
import * as jwt from 'jsonwebtoken';


const app = express();  // err - express()
const port = 3000;

app.get('/api', (req:Request, res:Response) => {
    res.json({
        message: 'Hi there! Welcome to my API service'
    });
});

app.post('/api/posts', verifyToken, (req:Request, res:Response) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {  // err - token, err-type, authData-type
        if(err) {
            res.sendStatus(403) //forbidden
        }else {
            res.json( {
                message: 'Post created...',
                authData
            });
        }
    });

});

interface arg {
    name: string,
    email: string,
    phoneNumber: number,
    age?: number,
}



app.post('/api/login', (req:Request, res:Response) => {
    let user: arg = {
        name: 'Sam',
        email: 'Zaqaryan.sam@gmail.com',
        phoneNumber: +37477433320,
        age: 21
    }

    jwt.sign({user}, 'secretkey', (err, token) =>{  //err - err-type, token-type    allow only type:any
        res.json( {
            token,
        });
    });
});

    function verifyToken(req:Request, res:Response, next:NextFunction) {
        const bearerHeader = req.headers['authorization']
        if(typeof bearerHeader !== 'undefined') {
            const bearerToken = bearerHeader.split(' ')[1]
            req.token = bearerToken    // err - token
            next()
        }else {
            res.sendStatus(403) //forbidden
        }
    }

    app.listen(port, (req:Request, res:Response) => {
        console.log(`server started on portt ${port}`)
})
