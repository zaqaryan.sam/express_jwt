"use strict";
exports.__esModule = true;
var express = require("express");
var jwt = require("jsonwebtoken");
// import {verify} from "jsonwebtoken";
var app = express();
var port = 3000;
app.get('/api', function (req, res) {
    res.json({
        message: 'Hi there! Welcome to my API service'
    });
});
app.post('/api/posts', verifyToken, function (req, res) {
    jwt.verify(req.token, 'secretkey', function (err, authData) {
        if (err) {
            res.sendStatus(403); //forbidden
        }
        else {
            res.json({
                message: 'Post created...',
                authData: authData
            });
        }
    });
});
app.post('/api/login', function (req, res) {
    var user = {
        name: 'Sam',
        email: 'Zaqaryan.sam@gmail.com',
        phoneNumber: +37477433320,
        age: 21
    };
    jwt.sign({ user: user }, 'secretkey', function (err, token) {
        res.json({
            token: token
        });
    });
});
function verifyToken(req, res, next) {
    var bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        var bearerToken = bearerHeader.split(' ')[1];
        req.token = bearerToken;
        next();
    }
    else {
        res.sendStatus(403); //forbidden
    }
}
app.listen(port, function (req, res) {
    console.log("server started on portt " + port);
});
